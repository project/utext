<?php

namespace Drupal\utext\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextareaWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'utext_plain_area_widget' widget.
 *
 * @FieldWidget(
 *   id = "utext_plain_area_widget",
 *   label = @Translation("Textarea (UText)"),
 *   field_types = {
 *     "string_long"
 *   },
 * )
 */
class UtextPlainAreaWidget extends StringTextareaWidget {
    
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'filter_options' => [
            'filter_utf8' => 'filter_utf8',
            'decode_entities' => 'decode_entities',
            'simplify_dashes' => 'simplify_dashes',
            'replace_quotes' => 'replace_quotes',
            'replace_triple_dots' => 'replace_triple_dots',
            'simplify_spaces' => 'simplify_spaces',
            'collapse_spaces' => 'collapse_spaces',
            'trim' => 'trim',
            'normalize' => 'normalize',
            ]
    ] + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $element = parent::settingsForm($form, $form_state);
    $element['filter_options'] = \Drupal::service('utext.plain')
            ->getOptionsElement($current_options, $this->t('Filter Options'));
    return $element;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $sum = [];
    foreach ($current_options as $k => $v) if (!empty($v)) $sum[] = $k;
    $sum = implode(', ', $sum);
    if (!empty($sum)) $summary[] = $this->t('Filter Options').': '.$sum;
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if (empty($element['value']['#element_validate'])) 
        $element['value']['#element_validate'] = [];
    array_unshift($element['value']['#element_validate'],
        [$this, 'valueFilterCallback']);
    $options = $this->getSetting('filter_options');
    $element['value']['#default_value']
        = \Drupal::service('utext.plain')
        ->escape_filter($element['value']['#default_value'], $options);
    return $element;
  }
  
  /**
   * Element validation
   */
  public function valueFilterCallback($element, &$form_state, $form) {
    if (!empty($element['#value'])) {
        $options = $this->getSetting('filter_options');
        if (empty($options)) $options = [];
        $lang_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $filter = \Drupal::service('utext.plain')
            ->setLangId($lang_id)
            ->setOptions($options);
        $filtered = $filter->filter($element['#value']);
        $form_state->setValueForElement($element,$filtered);
    }
  }

}
