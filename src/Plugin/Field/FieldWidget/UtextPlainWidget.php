<?php

namespace Drupal\utext\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'utext_plain_widget' widget.
 *
 * @FieldWidget(
 *   id = "utext_plain_widget",
 *   label = @Translation("Textfield (UText)"),
 *   field_types = {
 *     "string"
 *   },
 * )
 */
class UtextPlainWidget extends StringTextfieldWidget {
    
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'filter_options' => [
            'filter_utf8' => 'filter_utf8',
            'decode_entities' => 'decode_entities',
            'simplify_dashes' => 'simplify_dashes',
            'replace_quotes' => 'replace_quotes',
            'replace_triple_dots' => 'replace_triple_dots',
            'simplify_spaces' => 'simplify_spaces',
            'collapse_spaces' => 'collapse_spaces',
            'trim' => 'trim',
            'normalize' => 'normalize',
            ],
        'filter_lettercase' => '',
        'filter_pattern' => '',
    ] + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $element['filter_options'] = \Drupal::service('utext.plain')
            ->getOptionsElement($current_options, $this->t('Filter Options'));
    $letter_case = $this->getSetting('filter_lettercase');
    if (empty($letter_case)) $letter_case = ''; 
    $element['filter_lettercase'] = [
        '#type' => 'select',
        '#title' => $this->t('Letter case'),
        '#default_value' => $letter_case,
        '#options' => [
            '' => 'none',
            'L' => 'lower',
            'U' => 'upper',
            'T' => 'title',
            ],
        ];
    $pat = $this->getSetting('filter_pattern');
    if (empty($pat)) $pat = '';
    else $pat = trim($pat);
    $element['filter_pattern'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pattern'),
        '#description' => $this->t('<a href="https://www.php.net/manual/en/function.preg-match.php" target="_blank">Regular expression</a> pattern.<br>Leave blank to switch off pattern validation.'),
        '#default_value' => $pat,
        '#size' => 60,
        ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $sum = [];
    foreach ($current_options as $k => $v) if (!empty($v)) $sum[] = $k;
    $sum = implode(', ', $sum);
    if (!empty($sum)) $summary[] = $this->t('Filter Options').': '.$sum;
    $cases = [
        'L' => 'Lower case',
        'U' => 'Upper case',
        'T' => 'Title case',
        ];
    $sum = [];
    $case = $this->getSetting('filter_lettercase');
    if (!empty($case) && array_key_exists($case,$cases)) $sum[] = $cases[$case];
    $pat = $this->getSetting('filter_pattern');
    if (!empty($pat)) $pat = trim($pat);
    if (!empty($pat)) $sum[] = $this->t('Pattern');
    if (!empty($sum)) $summary[] = implode(', ', $sum);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if (empty($element['value']['#element_validate'])) 
        $element['value']['#element_validate'] = [];
    array_unshift($element['value']['#element_validate'],
        [$this, 'valueFilterCallback']);
        
    $options = $this->getSetting('filter_options');
    $element['value']['#default_value']
        = \Drupal::service('utext.plain')
        ->escape_filter($element['value']['#default_value'], $options);
    return $element;
  }


  /**
   * Element validation
   */
  public function valueFilterCallback($element, &$form_state, $form) {
    if (!empty($element['#value'])) {
        $options = $this->getSetting('filter_options');
        if (empty($options)) $options = [];
        $lang_id = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $filter = \Drupal::service('utext.plain')
            ->setLangId($lang_id)
            ->setOptions($options);
        $filtered = $filter->filter($element['#value']);
        $letter_case = $this->getSetting('filter_lettercase');
        if (!empty($letter_case)) {
            switch ($letter_case) {
                case 'L': { $filtered = mb_strtolower($filtered,'UTF-8'); break; }
                case 'U': { $filtered = mb_strtoupper($filtered,'UTF-8'); break; }
                case 'T': { $filtered = mb_convert_case($filtered, MB_CASE_TITLE, 'UTF-8'); break; }
            }
        }
        $pat = $this->getSetting('filter_pattern');
        if (!empty($pat)) $pat = trim($pat);
        if (!empty($pat)) {
            if (preg_match($pat, $filtered)!==1) {
                $form_state->setError($element, $this->t('Value is not match the pattern!'));
                return;
            }
        }
        $form_state->setValueForElement($element,$filtered);
    }
  }

}
