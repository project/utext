<?php

namespace Drupal\utext\Plugin\Field\FieldFormatter;

//use Drupal\Core\Field\FormatterBase;
use Drupal\text\Plugin\Field\FieldFormatter\TextTrimmedFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'utext_plain_trimmed' formatter.
 *
 * @FieldFormatter(
 *   id = "utext_plain_trimmed",
 *   label = @Translation("Trimmed plain (UText)"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   quickedit = {
 *     "editor" = "form"
 *   }
 * )
 */
class UtextPlainTrimmedFormatter extends TextTrimmedFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'filter_options' => [
            'filter_utf8' => 'filter_utf8',
            'newline_tags'=> 'newline_tags',
            'strip_tags'  => 'strip_tags',
            'replace_quotes' => 'replace_quotes',
            'replace_triple_dots' => 'replace_triple_dots',
            'simplify_spaces' => 'simplify_spaces',
            'collapse_spaces' => 'collapse_spaces',
            'trim' => 'trim',
            'normalize' => 'normalize',
        ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $element['filter_options'] = \Drupal::service('utext.plain')
            ->getOptionsElement($current_options, $this->t('Summary Filter Options'));

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $sum = [];
    foreach ($current_options as $k => $v) if (!empty($v)) $sum[] = $k;
    $sum = implode(', ',$sum);
    if (!empty($sum)) $summary[] = $this->t('Summary Filter Options').': '.$sum;
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $options = $this->getSetting('filter_options');
    if (empty($options)) $options = [];
    $trim_length = $this->getSetting('trim_length');
    $filter = \Drupal::service('utext.plain')->setOptions($options);
    foreach ($items as $delta => $item) {
        $summary = NULL;
        if (!empty($item->summary)) {
            $summary = $item->summary;
        } else {
            $summary = text_summary($item->value, $item->format, $trim_length);
        }
        $s= $filter->setLangId($item->getLangcode())->filter($summary);
        $elements[$delta] = [
            '#plain_text' => $s,
        ];
    }

    return $elements;
  }

}
