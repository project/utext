<?php

namespace Drupal\utext\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'utext_plain_formatter'.
 *
 * @FieldFormatter(
 *   id = "utext_plain_formatter",
 *   label = @Translation("Plain (UText)"),
 *   field_types = {
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class UtextPlainFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'filter_options' => [
            'filter_utf8' => 'filter_utf8',
            'replace_quotes' => 'replace_quotes',
            'replace_triple_dots' => 'replace_triple_dots',
            'simplify_spaces' => 'simplify_spaces',
            'collapse_spaces' => 'collapse_spaces',
            'trim' => 'trim',
            'normalize' => 'normalize',
            ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $element = [
        'filter_options' => \Drupal::service('utext.plain')
            ->getOptionsElement($current_options, $this->t('Filter Options')),
        ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $current_options = $this->getSetting('filter_options');
    if (empty($current_options)) $current_options = [];
    $sum = [];
    foreach ($current_options as $k => $v) if (!empty($v)) $sum[] = $k;
    $sum = implode(', ',$sum);
    if (!empty($sum)) $summary[] = $this->t('Filter Options').': '.$sum;
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $options = $this->getSetting('filter_options');
    if (empty($options)) $options = [];
    // always setup special char escapement:
    
    $filter = \Drupal::service('utext.plain')->setOptions($options);
    foreach ($items as $delta => $item) {
        $s = $item->value;
        $s = $filter->setLangId($item->getLangcode())->filter($s);
        $elements[$delta] = [
            '#plain_text' => $s
        ];
    }
    return $elements;
  }

}
