<?php

namespace Drupal\utext;

use infoxy\utext\PlainFilter;

class PlainText extends PlainFilter {

    public static function getOptionsElement($current_options, $title = 'Filter Options') {
        $options = [
            'filter_utf8' => t('Bypass only correct utf8 chars'),
            'newline_tags'=> t('Insert "\r\n" before every "&lt;" (Useful with strip tags)'),
            'strip_tags'  => t('Strip tags'),
            'decode_entities' => t('Decode html entities to chars'),

            'lang_quotes' => t('Replace double quotes with language-specific ones'),

            'simplify_dashes' => t('Simplify hyphens and dashes to hyphen-minus, endash or emdash'),

            'shy_pattern'     => t('Use soft-hyphen pattern \-'),
            'dash_patterns'   => t('Use dash patterns -- and ---'),

            'replace_triple_dots' => t('Replace triple dots with ellipsis'),
            'replace_quotes'  => t('Replace single and double quotes with curly quotes'),
            'replace_specials'=> t('Replace special chars with safe ones'),

            'simplify_spaces' => t('Simplify spaces (Replace nbsp and spations)'),
            'collapse_spaces' => t('Replace sequence of whitespaces with single space'),
            'zebra_spaces' => t('Replace space pairs with nbsp+space'),

            'trim' => t('Trim leading and trailing whitespaces'),
            'trim_dots'   => t('Trim leading and trailing dots'),

            'normalize' => t('Normalize')
        ];
        $current = (empty($current_options)) ? [] : $current_options;
        $element = [
        '#type' => 'checkboxes',
        '#title' => $title,
        '#options' => $options,
        '#default_value' => $current,
        ];
        return $element;
    }

}
